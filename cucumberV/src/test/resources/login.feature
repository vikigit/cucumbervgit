Feature: Login functionality

  Scenario: Login with valid credentials
   Given I have open the browser
    When User enter username and password
    Then User is redirected to Dashboard

  Scenario: Login with invalid credentials
    Given I have open the browser
   When User enters invalid username and invalid password
    Then Error allert should be shown

  Scenario: Login with empty credentials
    Given I have open the browser
    When User click on login button without entering username and password
   Then Error allert should be displayed

  Scenario: Login with empty username and valid password
    Given I have open the browser
    When User enter only valid password
    Then Error allert should be displayed

  Scenario: Login with valid username and empty password
    Given I have open the browser
    When User enter only valid username
   Then Error allert should be displayed

  Scenario: Verify "Lost your password?" is available link
    Given I have open the browser
    When User click on "Lost your password?" link
    Then User should be redirected to a page for reseting password
