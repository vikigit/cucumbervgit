package stepDefinitions;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.Hooks;

public class SearchSteps {
	public WebDriver driver = new ChromeDriver();
	public Hooks hooksObj = new Hooks(driver);
	
	@Given("Browser is open")
	public void openBrowser() {
		hooksObj.testSetUp();
		driver.findElement(By.xpath("//a[@class=\"tools-icon my-account-icon \"]")).click();
	
	}
	@When("We entering username and password")
	public void insertValidUsernameAndPassword() {
	    driver.findElement(By.xpath("//input[@id='username']")).sendKeys("user1");
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys("testinginjira2022");
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	}
	@Then("We are redirected to Dashboard")
	public void user_is_redirected_to_dashboard() {
	    Boolean isDisplayed = driver.findElement(By.xpath("//a//child::span[@class=\"woostify-svg-icon icon-dashboard\"]")).isDisplayed();
	    assertTrue(isDisplayed);
	}
	@Given("User click on Search Icon")
	public void clickOnSearch() {
		driver.findElement(By.xpath("//span[@class='woostify-svg-icon icon-search']//ancestor::div[@class='site-tools']")).click();
		
	}
	@When("He type some product name and press enter")
	public void typeProductName() {
		WebElement search = driver.findElement(By.xpath("(//input[@class='search-field'])[1]"));
		search.clear();
		search.sendKeys("Hoodie");
		driver.findElement(By.xpath("(//button[text()='Search'])[2]")).click();
		
	}
	@Then("User is redirected to PLP with correct products")
	public void verifyUserIsRedirectedToPLP() {
		Boolean isPresent = driver.findElement(By.xpath("//select[@name='orderby']")).isDisplayed();
		assertTrue(isPresent);
	}
}
