package stepDefinitions;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.Hooks;

public class LoginSteps {
	public WebDriver driver = new ChromeDriver();
	public Hooks hooksObj = new Hooks(driver);
	
	@Given("I have open the browser")
	public void openBrowser() {
		hooksObj.testSetUp();
		driver.findElement(By.xpath("//a[@class=\"tools-icon my-account-icon \"]")).click();
	
	}
	//@When("^User enters (.*) and (.*)$")
	@When("User enter username and password")
	public void insertValidUsernameAndPassword() {
	    driver.findElement(By.xpath("//input[@id='username']")).sendKeys("user1");
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys("testinginjira2022");
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	    
	}
	@Then("User is redirected to Dashboard")
	public void user_is_redirected_to_dashboard() throws InterruptedException {
	    Boolean isDisplayed = driver.findElement(By.xpath("//a//child::span[@class=\"woostify-svg-icon icon-dashboard\"]")).isDisplayed();
	    assertTrue(isDisplayed);
	    hooksObj.tesTearDown();
	}
	
	@When("User enters invalid username and invalid password")
	public void insertInvalidUsernameAndPassword() {
	    driver.findElement(By.xpath("//input[@id='username']")).sendKeys(".#4%");
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys("FFgHH");
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	}
	@Then("Error allert should be shown")
	public void errorAllert() throws InterruptedException {
	    Boolean isDisplayed = driver.findElement(By.xpath("//ul[@class='woocommerce-error']")).isDisplayed();
	    assertTrue(isDisplayed);
	    hooksObj.tesTearDown();
	}
	@When("User click on login button without entering username and password")
	public void clickOnLoginButton() {
	    
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	}
	@Then("Error allert should be displayed")
	public void usernameIsRequired() throws InterruptedException {
	  Boolean  error = driver.findElement(By.xpath("//li//strong")).isDisplayed();
	   
	    assertTrue(error);  
	    hooksObj.tesTearDown();
	}
	@When("User enter only valid password")
	public void insertValidPasswordWithEmptyUsername() {
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("testinginjira2022");
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	}
	/*
	@And("User click on Rememberme")
	public void clickRememberme() {
		//WebElement rememberme = driver.findElement(By.xpath("//input[@id='rememberme']"));
		//rememberme.click();
		//assertTrue(rememberme.isSelected());
		
		WebElement checkBoxElement = driver.findElement(By.xpath("//input[@id='rememberme']"));
		boolean isSelected = checkBoxElement.isSelected();
				
	
		if(isSelected == false) {
			checkBoxElement.click();
		}
	    driver.findElement(By.xpath("//input[@id='rememberme']")).click();
	    driver.findElement(By.xpath("//button[@name='login']")).click();


	}
	*/
	@When("User enter only valid username")
	public void insertValidUsernameWithEmptyPassword() {
	    driver.findElement(By.xpath("//input[@id='username']")).sendKeys("user1");
	    driver.findElement(By.xpath("//button[@name='login']")).click();
	}
	@When("User click on \"Lost your password?\" link")
	public void clickOnLostYourPasswordLink() {
		driver.findElement(By.xpath("//a[text()='Lost your password?']")).click();
	}
	@Then("User should be redirected to a page for reseting password")
	public void verifyUserIsOnResetingPasswordPage() throws InterruptedException {
		 Boolean resetPasswordButton = driver.findElement(By.xpath("//button[text()=\"Reset password\"]")).isDisplayed();
		    assertTrue(resetPasswordButton);
		    hooksObj.tesTearDown();
	}
	
	
}
