package stepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import objects.Hooks;

public class RegisterSteps {
	public WebDriver driver = new ChromeDriver();
	public Hooks hooksObj = new Hooks(driver);

	@Given("Open the browser")
	public void openTheBrowser() {
		hooksObj.testSetUp();
		driver.findElement(By.xpath("//a[@class=\"tools-icon my-account-icon \"]")).click();

	}

	@When("User enters username, email and very week password")
	public void insertValidUsernameEmailAndVeryWeekPassword() {
		driver.findElement(By.xpath("//input[@id='reg_username']")).sendKeys("Marija");
		driver.findElement(By.xpath("//input[@id='reg_email']")).sendKeys("hello123@test.com");
		driver.findElement(By.xpath("//input[@id='reg_password']")).sendKeys("123");

	}

	@Then("Register button is disabled")
	public void registerButtonDisabled() throws InterruptedException {
		 Boolean isDisplayed = driver.findElement(By.xpath("//button[@disabled=\"disabled\"]")).isDisplayed();
		    assertTrue(isDisplayed);
		
		hooksObj.tesTearDown();
	}

	@When("User enters username, email and week password")
	public void insertValidUsernameEmailAndWeekPassword() {
		driver.findElement(By.xpath("//input[@id='reg_username']")).sendKeys("Anita");
		driver.findElement(By.xpath("//input[@id='reg_email']")).sendKeys("abcdEFG@test.com");
		driver.findElement(By.xpath("//input[@id='reg_password']")).sendKeys("abc1");
	}
	@Then("Validation message for very week password should be shown")
	public void veryWeekPassword() throws InterruptedException {
		String actMessage = driver.findElement(By.xpath("//div[@class='woocommerce-password-strength short']")).getText();
		String expMessage = "Very weak - Please enter a stronger password.";
		assertEquals(expMessage, actMessage);
		hooksObj.tesTearDown();
	}
	@Then("Validation message for week password should be shown")
	public void weekPassword() throws InterruptedException {
		String actualString = driver.findElement(By.xpath("//div[@class='woocommerce-password-strength bad']")).getText();
		assertTrue(actualString.contains("Weak - Please enter a stronger password."));
		hooksObj.tesTearDown();
	}
	@When("User click on Register button with empy fields")
	public void clickRegisterWithEmptyFields() {
		driver.findElement(By.xpath("//button[text()='Register']")).click();
	}

	@Then("Error message should be shown")
	public void verifyErrorMessage() throws InterruptedException {
		 Boolean isDisplayed = driver.findElement(By.xpath("//li//strong")).isDisplayed();
		    assertTrue(isDisplayed);
		    hooksObj.tesTearDown();
	}
	@When("User enters valid username, email and password and click on Register")
	public void insertValidUsernameEmailAndPassword( ) {
		
		driver.findElement(By.xpath("//input[@id='reg_username']")).sendKeys("petrankakostova");
		driver.findElement(By.xpath("//input[@id='reg_email']")).sendKeys("petrastova@test.com");
		driver.findElement(By.xpath("//input[@id='reg_password']")).sendKeys("Petr$ankA123456");
		
		driver.findElement(By.xpath("//button[text()='Register']")).click();

	}
	@Then("User is redirected to Dashboard page")
	public void verifyUserIsRedirectedToDashboard() throws InterruptedException {
	    Boolean isDisplayed = driver.findElement(By.xpath("//a//child::span[@class=\"woostify-svg-icon icon-dashboard\"]")).isDisplayed();
	    assertTrue(isDisplayed);
	    hooksObj.tesTearDown();
	}
	@When("User enters valid username, invalid email, valid password and click on Register")
	public void insertValidUsernamePasswordAndInvalidEmail() {
		driver.findElement(By.xpath("//input[@id='reg_username']")).sendKeys("petraPet");
		driver.findElement(By.xpath("//input[@id='reg_email']")).sendKeys("petra");
		driver.findElement(By.xpath("//input[@id='reg_password']")).sendKeys("petraPet54321");
		driver.findElement(By.xpath("//button[text()='Register']")).click();

	}
	@Then("User is not redirected to Dashboard")
	public void verifyUserIsNotRedirectedToDashboard() throws InterruptedException {
		Boolean registerButton = driver.findElement(By.xpath("//button[text()='Register']")).isDisplayed();
	    assertTrue(registerButton);
	    hooksObj.tesTearDown();
	}
	@When("User enters valid username, email already present, valid password and click on Register")
	public void insertValidUsernameEmailAlreadyPresentAndPassword() {
		driver.findElement(By.xpath("//input[@id='reg_username']")).sendKeys("petrankakostova");
		driver.findElement(By.xpath("//input[@id='reg_email']")).sendKeys("testinginjira2022@gmail.com");
		driver.findElement(By.xpath("//input[@id='reg_password']")).sendKeys("petraPet54321");
		driver.findElement(By.xpath("//button[text()='Register']")).click();
	
	}
	@Then("Error message should be displayed")
	public void verifyMessageIsDisplayed() throws InterruptedException {
		 Boolean isDisplayed = driver.findElement(By.xpath("//li//strong")).isDisplayed();
		    assertTrue(isDisplayed);
		    hooksObj.tesTearDown();
	}
	
}
